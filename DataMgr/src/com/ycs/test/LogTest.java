package com.ycs.test;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.xml.DOMConfigurator;


public class LogTest {

	
	private final static Logger log = Logger.getLogger(LogTest.class);
	
	
	public static void main (String args[]){
		  log.trace("Trace Message!");//
	      log.debug("Debug Message!");//
	      log.info("Info Message!");
	      log.warn("Warn Message!");
	      log.error("Error Message!");
	      log.fatal("Fatal Message!");
	}
	
	
}
