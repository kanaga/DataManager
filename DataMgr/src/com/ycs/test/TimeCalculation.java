package com.ycs.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TimeCalculation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String timeStamp = new SimpleDateFormat("HH:mm:ss:SSS").format(Calendar.getInstance().getTime());
		System.out.println(timeStamp);
		
		String time1 = "17:06:04:419";
		String time2 = "17:06:04:819";
		System.out.println(getDifference(timeStamp, timeStamp));
	}
	
	public static long getDifference(String time1, String time2){
		SimpleDateFormat formater = new SimpleDateFormat("HH:mm:ss:SSS");
		Date fromDate = null;
		Date toDate = null;
		try {
			fromDate = formater.parse(time1);
			toDate = formater.parse(time2);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		long difference = toDate.getTime() - fromDate.getTime(); 
		return difference;
		
	}

}
