package com.ycs.dm;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.io.StringWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.sql.DataSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.ycs.dm.bean.DMTxnParams;
import com.ycs.dm.helpers.DMRouterProperties;
import com.ycs.dm.helpers.DMTxnLog;


public class DataManager 
{
	private static final Logger logger = Logger.getLogger(DataManager.class);
	private static final Pattern pattern = Pattern.compile("<!ELEMENT\\s*(\\w*)\\s*\\(\\s*([\\s\\S]*)\\).*>",Pattern.MULTILINE);
	private static final StringBuffer filebuf = new StringBuffer(100);
	private static final HashMap<String, String> dtdmap = new HashMap<String, String>();
	private static final DocumentBuilderFactory docBuildFactory = DocumentBuilderFactory.newInstance();
	
	private static Document xmlDoc;
	
	private DataSource dataSource;
	private DMRouterProperties dmRouter;
	private DMTxnParams dmTxnParams;

	private String retString = "";
	private String routerKey;
	
	private  String dtdpath;
	private  String resfolder;
	
	public DataManager(String dbName) 
	{
		routerKey=dbName;
		logger.debug("DBName["+routerKey+"]");
		dmRouter = DMRouterProperties.getInstance();
		dataSource = dmRouter.getDMDataSource(routerKey);
	
		dtdpath = dmRouter.getReqDTDPath(routerKey);
		logger.trace("DTDPath ["+dtdpath+"]");
		resfolder = dmRouter.getResDTDPath(routerKey);
		logger.trace("RESFolder["+resfolder+"]");
	}

	/**
	 * @param elementName
	 *            - current node name retreived from DTD
	 * @param cxmlnode
	 *            - client supplied xml's node
	 * @throws Exception
	 */
	private void recurseDoc(String elementName, Element cxmlnode)
			throws Exception {

		boolean plussign = false;
		boolean starsign = false;
		boolean qsign = false;
		String containedElm = dtdmap.get(elementName);
		// debug(1,elementName);
		if (containedElm == null) {
			return;
		} else if (containedElm.trim().startsWith("#PCDATA")) {
			logger.trace(elementName + "=" + cxmlnode.getTextContent().trim());
			if (elementName.startsWith("FF")) {
				elementName = elementName.substring(1);
			}
			retString += elementName
					+ String.format("%03d", cxmlnode.getTextContent().trim()
							.length()) + cxmlnode.getTextContent().trim();
			return;
		}

		StringTokenizer st = new StringTokenizer(dtdmap.get(elementName), ", ");
		while (st.hasMoreTokens()) {
			plussign = false;
			starsign = false;
			qsign = false;
			String strnode = st.nextToken();
			if (strnode.matches(".*\\+$")) {
				plussign = true;
				strnode = strnode.substring(0, strnode.length() - 1);
			}
			if (strnode.matches(".*\\*$")) {
				starsign = true;
				strnode = strnode.substring(0, strnode.length() - 1);
			}
			if (strnode.matches(".*\\?$")) {
				qsign = true;
				strnode = strnode.substring(0, strnode.length() - 1);
			}

			NodeList elm = null;
			try {
				elm = cxmlnode.getElementsByTagName(strnode);
			} catch (Exception e) {
				logger.error("Exception for[" + strnode + "]:-" + e);
				continue;
			}

			if (plussign) {
				// if(elm.getLength() == 0)throw new
				// Exception("Multiple rows expected according to input file DTD, plus(+) sign detected");
				for (int i = 0; i < elm.getLength(); i++) {
					recurseDoc(strnode, (Element) elm.item(i));
				}

			} else if (starsign) {
				for (int i = 0; i < elm.getLength(); i++) {
					recurseDoc(strnode, (Element) elm.item(i));
				}

			} else if (qsign) {
				if (elm.item(0) != null)
					recurseDoc(strnode, (Element) elm.item(0));
			} else {
				if (!plussign && !starsign && elm.getLength() == 0 && !qsign)
					throw new Exception(
							" Some input error Element expected in input xml for node <"
									+ strnode
									+ "> according to DTD but it is not found");
				recurseDoc(strnode, (Element) elm.item(0));
			}
		}
	}

	public String createStrfrmXml(String clientInputStr) {

		String multipleInpDelimStart = null;
		try 
		{
			DocumentBuilder dbuild = docBuildFactory.newDocumentBuilder();
			InputStream xmlReStream = new ByteArrayInputStream(clientInputStr.getBytes());
			
			xmlDoc = dbuild.parse(xmlReStream);
			
			logger.trace("Request Received");
			
			xmlReStream.close();
			xmlReStream = null;
			
			String rootStr = "";
			rootStr = xmlDoc.getDocumentElement().getNodeName();

			String trans_code = xmlDoc.getElementsByTagName("TRANS_CODE").item(0).getTextContent();
			
			logger.trace("PLSQL TXN CODE:-"+trans_code);
			
			dmTxnParams = DMRouterProperties.getDMTxnParams(trans_code,routerKey);
			
			
			if (dmTxnParams==null) {
				logger.error("Configuration for [" + trans_code + "] missing!");
				throw new Exception("Configuration not found for ["+ trans_code + "]");
			}
			logger.trace("DTD File["+dmTxnParams.txnDtdFilename+"]");
			BufferedReader fr = new BufferedReader(new FileReader(dtdpath+dmTxnParams.txnDtdFilename));
			String line = "";
			boolean found = false;
			boolean ismatched = false;
			String oldLine = "";
			while ((line = fr.readLine()) != null) 
			{
				filebuf.append(line);
				Matcher matcher = pattern.matcher(oldLine + line);
				while (matcher.find())
				{
					ismatched = true;
					/*
					 * for (int i = 1; i <= matcher.groupCount(); i++) { String
					 * string = matcher.group(i); }
					 */
					String childStr = matcher.group(2);
					if (childStr.trim().equals("#PCDATA")) {

					} else {
						StringTokenizer st = new StringTokenizer(childStr, ", ");

						while (st.hasMoreTokens()) {
							String strnode = st.nextToken();//
							if (found && multipleInpDelimStart == null) {
								multipleInpDelimStart = strnode;
								multipleInpDelimStart = multipleInpDelimStart
										.replaceAll("[\\+\\*\\?]", "");
								multipleInpDelimStart = multipleInpDelimStart
										.substring(1); // Convert FFxxxx to
								// Fxxxx
							}

						}
					}

					if (!found && rootStr.equals(matcher.group(1).trim())) {
						found = true;

					}
					dtdmap.put(matcher.group(1), matcher.group(2));

				}
				if (!ismatched) {
					oldLine += line;
				} else
					oldLine = "";
			}
			fr.close();
			fr = null;

			if (!found) {
				logger.warn("No match found for the root element in DTD");
				throw new Exception("Root element not found in DTD");
			}

			
			logger.trace("Constructing PLSQL Data elements");
			recurseDoc(rootStr, xmlDoc.getDocumentElement());
			logger.trace("PLSQL Request msg["+retString+"]");
		} catch (Exception e) 
		{
			logger.error("General Exception|" + e); 
			e.printStackTrace();
		}

		// /End of format input data
		try {
			return delegateSQLCall(retString, multipleInpDelimStart,clientInputStr);
		} catch (Exception e) 
		{
			logger.error("PLSQL Processing exception|" + e);
			logger.fatal("Exception in createStrfrmXml", e);
			clientInputStr = null;
			return clientInputStr;
		}
	}

	private String delegateSQLCall(String retString, String multipleInpDelim, String requestXML)
			throws Exception {

		Connection connection = null;
		CallableStatement ocStatement = null;
		ResultSet resultSet = null;
		String tobereturned = null;
		String retstrFirstPart = null;
		String reminder = null; // in out
		String header = null; // MultiMain in
		String procallStr = null;
		int retval = -1;
		logger.trace("Processing PLSQL Txn type["+dmTxnParams.txnType+"][Txn code"+dmTxnParams.txnCode+"]");

		String assemblepartIdct_Data = "";

		if (dmTxnParams.txnType == 10) {
			logger.warn("PLSQL call forTxn Ttype 10 is unsupported as of now") ;
			throw new Exception(
					"PLSQL call for type 10 is unsupported as of now");
		} else if (dmTxnParams.txnType == 11) 
		{
			logger.trace("Before calling TxnMainPkg.MultiBlockMai for Txn type 11") ;
            logger.info("11|PLIN|"+DMTxnLog.getXMLParsedata(requestXML).trim()+"11|"+dmTxnParams.txnCode+"|");
            logger.info("11|PLIN_DATA|"+DMTxnLog.getXMLParsedata(requestXML).trim()+dmTxnParams.txnCode+"|"+retString.trim());

			
			procallStr = "{call TxnMainPkg.MultiBlockMain(?,?,?,?,?,?)}";
			try {
				connection = dataSource.getConnection();
				logger.debug("After establising Connection..");	
				ocStatement =  connection.prepareCall(procallStr);
				int numrun = Integer.parseInt(dmTxnParams.txnOutputRes);
				logger.debug("Number of Response["+numrun+"]");
				for (int i = 1; i <= numrun; i++) 
				{
					ocStatement.setString(1, dmTxnParams.txnCode);
					ocStatement.setString(2, retString);
					ocStatement.setInt(6, i);
					ocStatement.registerOutParameter(3, OracleTypes.CURSOR);
					logger.debug("After getting cursor");
					ocStatement.registerOutParameter(4, Types.VARCHAR);
					ocStatement.registerOutParameter(5, Types.NUMERIC);

					logger.debug("PLSQL Call Inputs TransCode:["+ dmTxnParams.txnCode + "] TransData:[" + retString+ "] ProcNumber:[" + i + "]");
					ocStatement.execute();

					retstrFirstPart = ocStatement.getString(4);
					retval = ocStatement.getInt(5);

					logger.trace("PLSQL Result["	+ retstrFirstPart + "] RetVal:[" + retval + "]");
					
					
					ArrayList<String> cursorar = new ArrayList<String>();

					if (retval > -1) {
						resultSet = (ResultSet) ocStatement.getObject(3);
						//ResultSetMetaData rsm = resultSet.getMetaData();

						String temp;
						//String colname;
						//String coltype;

						while (resultSet.next()) 
						{
							int j = 0;
							temp = (String) resultSet.getObject(j + 1);
							if (temp == null)
								temp = "NO_DATA";
							//colname = rsm.getColumnName(j + 1);
							//coltype = rsm.getColumnTypeName(j + 1);
							//logger.trace("PLSQL Cursor data[" + temp+"]");
							cursorar.add(temp);

							// }

						}

						logger.trace("PLSQL Return Result Size["+cursorar.size() + "]Cursor Data[" + cursorar+"]");
						logger.info("11|PLOUT_DATA|"+DMTxnLog.getXMLParsedata(requestXML).trim()+dmTxnParams.txnCode+"|"+cursorar+"|");
						logger.info("11|PLOUT|"+DMTxnLog.getXMLParsedata(requestXML).trim()+dmTxnParams.txnCode.trim()+"|"+retstrFirstPart.trim() +"|");
   
					} else {
						assemblepartIdct_Data = "";
						break;
					}
					dmTxnParams.txnOutputRes = dmTxnParams.txnName + i + ".res";
					assemblepartIdct_Data += formatResString(retstrFirstPart,cursorar);
					dmRouter.closeDBElements(resultSet, null, null);
				}

			} catch (Exception e) 
			{
				logger.error("Exception in DelegateSQL 0:-" + e);
			} finally {
				dmRouter.closeDBElements(resultSet, ocStatement, connection);
				procallStr = null;
			}

			boolean removeOldIdct_Data = true;
			if (retval < 0 && retstrFirstPart.contains("XML_ERROR"))
				removeOldIdct_Data = false;
			tobereturned = assembleResult(retstrFirstPart,assemblepartIdct_Data, removeOldIdct_Data);
			// throw new
			// Exception("PLSQL call for type 11 is unsupported as of now");
		} else if ((dmTxnParams.txnType != 4 && dmTxnParams.txnType != 5 && dmTxnParams.txnType != 6))
		{ // 1,2,3,7,8,
			logger.trace("Before calling TxnMainPkg.MultiBlockMain for Txn type["+dmTxnParams.txnType+"] of  4/5/6  ");

            logger.info(dmTxnParams.txnType+"|PLIN|"+DMTxnLog.getXMLParsedata(requestXML).trim()+dmTxnParams.txnType+"|"+dmTxnParams.txnCode+"|");

            logger.info(dmTxnParams.txnType+"|PLIN_DATA|"+DMTxnLog.getXMLParsedata(requestXML).trim()+dmTxnParams.txnCode+"|"+retString.trim());
			procallStr = "{call TxnMainPkg.Main(?,?,?,?,?)}";;
			connection = dataSource.getConnection();
			ocStatement =  connection.prepareCall(procallStr);
			try {
				ocStatement.setString(1, dmTxnParams.txnCode);
				ocStatement.setString(2, retString);

				ocStatement.registerOutParameter(3, OracleTypes.CURSOR);
				ocStatement.registerOutParameter(4, Types.VARCHAR);
				ocStatement.registerOutParameter(5, Types.NUMERIC);
				logger.debug("PLSQL Call Inputs TransCode:["+ dmTxnParams.txnCode + "] TransData:[" + retString+ "]");
				ocStatement.execute();

				retstrFirstPart = ocStatement.getString(4);
				retval = ocStatement.getInt(5);
				logger.trace("PLSQL result after execution: RetStr["+ retstrFirstPart + "] RetVal[" + retval + "]");
				
				ArrayList<String> cursorar = new ArrayList<String>();
				boolean cursordatafound = false;

				if (retval > -1) {
					resultSet = (ResultSet) ocStatement.getObject(3);

					String temp;

					while (resultSet.next()) {
						// for (int j=0;j< columnCount;j++){
						int j = 0;
						temp = (String) resultSet.getObject(j + 1);
						if (temp == null)
							temp = "NO_DATA";
						cursorar.add(temp);

						// }
						cursordatafound = true;
					}

					logger.trace("PLSQL Return Result Size["+cursorar.size() + "]Cursor Data[" + cursorar+"]");
					
					logger.info(dmTxnParams.txnType+"|PLOUT_DATA|"+DMTxnLog.getXMLParsedata(requestXML).trim()+dmTxnParams.txnCode+"|"+cursorar +"|");
					 
					logger.info(dmTxnParams.txnType+"|PLOUT|"+DMTxnLog.getXMLParsedata(requestXML).trim()+dmTxnParams.txnCode+"|"+retstrFirstPart.trim() +"|");

					dmRouter.closeDBElements(resultSet, null, null);
				}

				boolean removeOldIdct_Data = true;
				if (!cursordatafound && retval > -1) {
					assemblepartIdct_Data = ""; // new <IDCT_DATA> wont be
					// inserted
					removeOldIdct_Data = true; // remove existing <IDCT_DATA> in
					// input XML
					retstrFirstPart = ""; // do change the header section
				}

				assemblepartIdct_Data = formatResString(retstrFirstPart,
						cursorar);

				if (dmTxnParams.txnType == 3) {
					removeOldIdct_Data = false; // Replace IDCT_DATA = no
					assemblepartIdct_Data = "";
				}

				logger.trace("DM Response String[" + assemblepartIdct_Data+"]");

				if (retval < 0 && retstrFirstPart.contains("XML_ERROR")) {
					removeOldIdct_Data = false;
				}

				tobereturned = assembleResult(retstrFirstPart,assemblepartIdct_Data, removeOldIdct_Data);

			} catch (Exception e) 
			{
				logger.error("Exception in delegateSQL:-" + e);
			} finally 
			{
				dmRouter.closeDBElements(resultSet, ocStatement, connection);
				procallStr = null;
			}

		} else 
		{// 4,5,6
			// TxnMainPkg.MultiMain(:TxnCode,:SrcStr,:Reminder,:RetStr,:Header);
			// MultiMain(TransCode number,TransData in out varchar2,Reminder
			// in out varchar2,RetStr in out varchar2,Header in varchar2);
			String[] strtok = retString.split("IDCT_MESSAGE_TYPE00201");
			boolean runflag = true;

			header = strtok[0] + "IDCT_MESSAGE_TYPE00201";
			String remainingStr = strtok[1];
			String SrcStr = null;
			retstrFirstPart = "2";
			logger.debug("Before calling TxnMainPkg.MultiBlockMain for Txn type["+dmTxnParams.txnType+"]"); 
			logger.info(dmTxnParams.txnType+"|PLIN|"+DMTxnLog.getXMLParsedata(requestXML).trim()+dmTxnParams.txnType+"|"+dmTxnParams.txnCode+"|");
            logger.info(dmTxnParams.txnType+"|PLIN_DATA|"+DMTxnLog.getXMLParsedata(requestXML).trim()+dmTxnParams.txnCode+"|"+retString.trim());

			procallStr = "{call TxnMainPkg.MultiMain(?,?,?,?,?)}";

			try {
				connection = dataSource.getConnection();
				ocStatement = connection.prepareCall(procallStr);
				for (int i = 0; runflag; i++) 
				{
					int a1 = remainingStr.indexOf(dmTxnParams.MutiInpDelim);
					if (remainingStr.split(dmTxnParams.MutiInpDelim).length > 2) 
					{
						int len = Integer.parseInt(remainingStr.substring(
								a1 + 5, a1 + 5 + 3));
						SrcStr = remainingStr.substring(0, a1 + 5 + 3 + len);
						retstrFirstPart = "7";
						runflag = true;
						remainingStr = remainingStr.substring(remainingStr
								.indexOf(dmTxnParams.MutiInpDelim) + 5 + 3 + len);
					} else
					{
						SrcStr = remainingStr;
						runflag = false;
						if (retstrFirstPart.charAt(0) != '2') 
						{
							retstrFirstPart = "0";
						}
					}
					ocStatement.setString(1, dmTxnParams.txnCode);
					ocStatement.setString(2, SrcStr);
					ocStatement.setString(3, reminder);
					ocStatement.setString(4, retstrFirstPart);
					ocStatement.setString(5, header);

					ocStatement.registerOutParameter(3, Types.VARCHAR);
					ocStatement.registerOutParameter(4, Types.VARCHAR);
					if (i == 0 && retstrFirstPart.charAt(0) != '2')
						retstrFirstPart = "1";
					// //////////////////Executing here //////////////////
					logger.debug("PLSQL Call Inputs TransCode:["+ dmTxnParams.txnCode + "] TransData:[" + retString+ "]MultiInpDelim["+ dmTxnParams.MutiInpDelim + "]" + " SrcStr[" + SrcStr+ "] header[" + header + "]RetStr:["+ retstrFirstPart + "]  Reminder:[" + reminder+ "]");
					
					ocStatement.execute();
					reminder = ocStatement.getString(3);
					retstrFirstPart = ocStatement.getString(4);

					logger.debug("PLSQL output after execution Reminder:["
							+ reminder + "] RetStr:[" + retstrFirstPart + "]");
					

					logger.info(dmTxnParams.txnType+"|PLOUT_DATA|"+DMTxnLog.getXMLParsedata(requestXML).trim()+dmTxnParams.txnCode+"|"+SrcStr.trim()+"|");
                    logger.info(dmTxnParams.txnType+"|PLOUT|"+DMTxnLog.getXMLParsedata(requestXML).trim()+dmTxnParams.txnCode+"|"+retstrFirstPart.trim()+"|");
					
					// assemblepart = formatResString(retstr,cursorar);

					String tmpIdctDataReminder = formatResStrPerReminder(reminder);

					// Append the reminder to the result XML - The assembling
					// (,,false) also retains the original IDCT_DATA
					tobereturned = assembleResult(retstrFirstPart, "", false);
					int spltidx = tobereturned.lastIndexOf("</IDCT_DATA>");
					String lastPart = tobereturned.substring(spltidx);
					if (retstrFirstPart != null
							&& retstrFirstPart.contains("XML_ERROR"))
						tmpIdctDataReminder = "";
					tobereturned = tobereturned.substring(0, spltidx)
							+ tmpIdctDataReminder + lastPart;
					tobereturned = tobereturned.replaceAll(">\\s<", "><");
					// replaceAll(">\\s*<", "><");
				}

			} catch (Exception e) {
				logger.error("Exception in delegateSQL 2:-" + e);
				logger.fatal("Exception in delegateSQLCall", e);
				
			} finally
			{
				dmRouter.closeDBElements(resultSet, ocStatement, connection);
				procallStr = null;
			}
			// throw new
			// Exception("PLSQL call for type "+cfg.txnType+" is getting implemented out of (4, 5, 6");
			// CTDSMultiInp();
		}
		return tobereturned;
	}

	private String formatResStrPerRes(String filename, String inpstr)
			throws Exception {
		String retStr = "";
		File f = new File(filename);
		BufferedReader onetimebf = null;
		if (f.exists())
			onetimebf = new BufferedReader(new FileReader(f));
		else
			throw new Exception("Cannot open file" + filename);

		String tmp;
		String dataField = null;
		int fieldlen = 0;
		boolean found = false;
		try {
			while ((tmp = onetimebf.readLine()) != null) {
				if (tmp.trim().equals(""))
					break;
				Pattern pattern = Pattern.compile(tmp.substring(1)
						+ "(\\d\\d\\d)");
				Matcher matcher = pattern.matcher(inpstr);
				found = false;

				if (matcher.find()) {
					fieldlen = Integer.parseInt(matcher.group(1));
					if (fieldlen > 0) {
						// Bug in PLSQL type 11 and proc no. 1 Txn Code 104 - it
						// gives more bytes in length thanactual length
						if (fieldlen > inpstr.substring(matcher.end()).length()) {
							// debug(3,"Possible error in plsql  Starting position:"+matcher.end()+" fieldlen:"+fieldlen+" inputsubstr length:"+inpstr.substring(matcher.end()).length()
							// );
							dataField = inpstr.substring(matcher.end());
						} else {
							dataField = inpstr.substring(matcher.end(),
									matcher.end() + fieldlen);
						}

						retStr += "<" + tmp + ">" + dataField + "</" + tmp
								+ ">\n";
						found = true;
					}
				}

				if (!found)
					retStr += "<" + tmp + ">NO_DATA</" + tmp + ">\n";

			}
			onetimebf.close();
			onetimebf = null;
		} catch (Exception e)
		{
			logger.error("Exception fieldlen[" + fieldlen + "]inpstr[" + inpstr	+ "]" + e);
			logger.fatal("Exception in formatResStrPerRes", e);
		}

		return retStr;

	}

	/**
	 * This method is called internally by
	 * {@link #delegateSQLCall(String, String)} to create string that goes
	 * inside &lt;IDCT_DATA>...&lt;/IDCT_DATA> This is for type 4, 5, 6. It
	 * worls for reminder
	 * 
	 * @param filename
	 * @param inpstr
	 *            Fxxxddddatadata.. supplied form PROC output
	 * @return
	 * @throws Exception
	 */
	private String formatResStrPerReminder(String inpstr) throws Exception {
		String retStr = "";
		if (inpstr == null)
			return "NO_DATA";
		String[] header = inpstr.split("\\|");

		for (int i = 0; i < header.length; i++) {
			if (header[i].contains("=")) {
				String[] key = header[i].split("=");
				if (key.length > 1)
				{
					retStr += "<" + key[0] + ">" + key[1] + "</" + key[0]
							+ ">\n";
				}
				else
				{
					retStr += "<" + key[0] + ">NO_DATA</" + key[0] + ">\n";
				}
			}
		}

		return retStr;

	}

	/**
	 * This is usually called by {@link #delegateSQLCall(String, String)} for
	 * final assembling of the result strings.
	 * 
	 * @param firstPartResStr
	 *            This is most important. It gets split by '|' and gets replaced
	 *            into incoming XML header
	 * @param retStrData
	 *            if this variable is "" or null then new
	 *            &lt;IDCT_DATA>..&lt;/IDCT_DATA> section is not inserted,
	 *            otherwise the existing string is split at &lt;/IDCT> and
	 *            &lt;IDCT_DATA>..&lt;/IDCT_DATA> is inserted and &lt;/IDCT> is
	 *            appended to the string
	 * @param removeIDCT_DATA
	 *            if this is false then existing &lt;IDCT_DATA>..&lt;/IDCT_DATA>
	 *            block is any does not gets removed
	 * @return
	 */
	private String assembleResult(String firstPartResStr, String retStrData,boolean removeIDCT_DATA) {
		// For trying example string
		Transformer transformer = null;
		StreamResult result = null;
		
		String resultxmlString = null;
		try 
		{
		logger.trace("firstPartResStr["+firstPartResStr+"]retStrData["+retStrData+"]removeIDCT_DATA["+removeIDCT_DATA+"]");		
			Element inpelmNode = xmlDoc.getDocumentElement();
		
			String[] header = firstPartResStr.split("\\|");
			logger.trace("firstPartResStr|"+firstPartResStr);
			logger.trace("retStrData|"+retStrData);
			for (int i = 0; i < header.length; i++) 
			{
				if (header[i].contains("=")) 
				{
					String key = header[i].split("=")[0];
				
					Element oldElement = (Element) inpelmNode.getElementsByTagName(key).item(0);
					Element newElement = xmlDoc.createElement(key);
					String value = header[i].split("=").length < 2 ? "NO_DATA": header[i].split("=")[1];
					if (oldElement != null)
					{
						newElement.setTextContent(value);
						inpelmNode.replaceChild(newElement, oldElement);
						
					}
										
				}
			
			}
			logger.debug("Removing existing IDCT_DATA of number|"+ inpelmNode.getElementsByTagName("IDCT_DATA").getLength());
			
			// transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			// transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION,
			// "yes");
			
			transformer = TransformerFactory.newInstance().newTransformer();
		    result = new StreamResult(new StringWriter());
		    DOMSource source = new DOMSource(inpelmNode);
		    transformer.transform(source, result);
			
			resultxmlString = result.getWriter().toString();
			// Removing Old IDCT_DATA
			logger.debug("Remove IDCT OLD DATA["+removeIDCT_DATA+"]");
			if (removeIDCT_DATA) 
			{
				resultxmlString = resultxmlString.replaceAll("<IDCT_DATA>[\\s\\S]*</IDCT_DATA>", "");
			}

			/*
			 * Merge newly previously created resultXml to this xmlstring
			 * essentially which means replace old <IDCT_DATA/> with new one
			 * which has result set
			 */
			if (retStrData != null && !"".equals(retStrData)) 
			{
				String[] temp = resultxmlString.split("</IDCT>");
				temp[0] += retStrData.toString() + "</IDCT>";
				resultxmlString = temp[0];
			}
			resultxmlString = resultxmlString.replaceAll("\\n", "");
			resultxmlString = resultxmlString.replaceAll(">\\s*<", "><");

		} catch (Exception e) 
		{
			logger.error("Exception Block " + e);
			logger.fatal("Exception in Construting Response back", e);
		}

		return resultxmlString;
	}

	/**
	 * Creates &lt;IDCT_DATA>....&lt;/IDCT_DATA> blocks based on data of
	 * cursorstrar. Calls {@link #formatResStrPerRes(String, String)} in loop
	 * first for cfg.txnOnetimeRes and following by multiple times
	 * cfg.txnOutputRes. This creates one &lt;IDCT_DATA>....&lt;/IDCT_DATA> per
	 * call. Finally these strings are appened and returned.
	 * 
	 * @param firstPartResStr
	 *            is unused as of time being
	 * @param cursorstrar
	 *            is the used thing to parse and based on .res files it creates
	 *            result XML part
	 * @return String of &lt;IDCT_DATA>....&lt;/IDCT_DATA> blocks.
	 */
	private String formatResString(String firstPartResStr,ArrayList<String> cursorstrar) 
	{

		StringBuffer retStr = new StringBuffer(100);
	
		try 
		{
			logger.trace("Resource OneTimeHeader|" + resfolder+ dmTxnParams.txnOnetimeRes);
			logger.trace("Resource BodyTags|" + resfolder + dmTxnParams.txnOutputRes);
			// Procedure XML_ERROR will cause this value to be 0
			if (cursorstrar.size() > 0)
			{
				if (dmTxnParams.txnOnetimeRes != null && !"".equals(dmTxnParams.txnOnetimeRes))
					retStr.append("<IDCT_DATA>").append(formatResStrPerRes(resfolder+ dmTxnParams.txnOnetimeRes, cursorstrar.get(0))).append("</IDCT_DATA>");

				if (dmTxnParams.txnOutputRes != null && !"".equals(dmTxnParams.txnOutputRes))
					for (Iterator <String>itr = cursorstrar.iterator(); itr.hasNext();) 
					{
						String string = itr.next();
						retStr.append("<IDCT_DATA>").append(formatResStrPerRes(resfolder+ dmTxnParams.txnOutputRes, string)).append("</IDCT_DATA>");
					}
			}
		} catch (Exception e) 
		{
			logger.error("Exception:-" + e);
			logger.fatal("Exception in formatResString", e);
		}
		return retStr.toString();
	}

	/**
	 * @param args
	 */

}
