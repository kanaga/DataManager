package com.ycs.dm.bean;

public class OutputArgument {
	
public OutputArgument(String datatype,String paramName) {
		this.paramName = paramName;
		this.datatype = datatype;
	}
public OutputArgument( String datatype) {
	this.datatype = datatype;
}
public String paramName;
public String datatype;
public String toString(){
	return "paramName:"+paramName+";datatype:"+datatype;
}
}
