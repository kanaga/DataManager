package com.ycs.dm.bean;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.SAXException;

public class InputArgument 
{
	private Logger logger = Logger.getLogger(InputArgument.class);
	private DocumentBuilderFactory dbf;
	private DocumentBuilder dbuild;

	public InputArgument(String paramName, String datatype, String data)
	{
		dbf = DocumentBuilderFactory.newInstance();
		try {
			dbuild = dbf.newDocumentBuilder();
		} catch (ParserConfigurationException e) 
		{
			logger.error("Exception 0:-"+e);
		}
		this.paramName = paramName;
		this.datatype = datatype;
		this.data = data;
	}

	public InputArgument(String datatype, String data) {
		dbf = DocumentBuilderFactory.newInstance();
		try {
			dbuild = dbf.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			logger.error("Exception 1:-"+e);
		}

		this.paramName = "";
		this.datatype = datatype;
		this.data = data;
	}

	private String paramName;
	public String datatype;
	private String data;

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getDatatype() {
		return datatype;
	}

	public void setDatatype(String datatype) {
		this.datatype = datatype;
	}

	public String getData() {

		InputStream reader = new ByteArrayInputStream(data.getBytes());
		// System.out.println("data:"+data);
		Document doc = null;
		try {
			doc = dbuild.parse(reader);
		} catch (SAXException e) 
		{
			logger.error("Exception 2:-"+e);
		} catch (IOException e)
		{
			logger.error("Exception 3:-"+e);
		}
		Element elmNode = doc.getDocumentElement();

		if ("STRUCT".equalsIgnoreCase(datatype)
				|| "ARRAY".equalsIgnoreCase(datatype)) {
			NodeList nl = elmNode.getChildNodes();
			for (int inode = 0; inode < nl.getLength(); inode++) {

				if (nl.item(inode).getNodeType() == Document.ELEMENT_NODE) {
					// StringWriter sw = new StringWriter();
					// OutputFormat format = OutputFormat.createPrettyPrint();
					// XMLWriter xw = new XMLWriter(sw, format);
					// try {
					// xw.write(doc);
					// } catch (IOException e) {
					// e.printStackTrace();
					// }
					// String result = sw.toString();
					//
					// return result;
					//
					//
					DOMImplementationRegistry registry = null;
					try {
						registry = DOMImplementationRegistry.newInstance();
					} catch (ClassCastException e) {

						logger.error("Exception 4:-"+e);
					} catch (ClassNotFoundException e) {

						logger.error("Exception 5:-"+e);
					} catch (InstantiationException e) {

						logger.error("Exception 6:-"+e);
					} catch (IllegalAccessException e) {

						logger.error("Exception 7:-"+e);
					}
					DOMImplementationLS lsImpl = (DOMImplementationLS) registry.getDOMImplementation("LS");
					LSSerializer serializer = lsImpl.createLSSerializer();
					LSOutput lso = lsImpl.createLSOutput();
					lso.setEncoding("UTF-8");
					StringWriter strw = new StringWriter();
					lso.setCharacterStream(strw);
					serializer.write(nl.item(inode), lso);

					return strw.getBuffer().toString();
				} else if (nl.item(inode).getNodeType() == Document.CDATA_SECTION_NODE) {

					return nl.item(0).getTextContent();
				}
			}

			return nl.item(0).getTextContent();

		} else {
			return elmNode.getTextContent();
		}
	}

	public void setData(String data) {
		this.data = data;
	}

	public String toString() {
		return "paramName:" + paramName + ";datatype:" + datatype + ";data:"
				+ getData();
	}
}
