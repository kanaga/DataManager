package com.ycs.dm.helpers;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.ycs.dm.bean.DMTxnParams;

public final class DMRouterProperties 
{
	private final static Logger Spool = Logger.getLogger(DMRouterProperties.class);
	private final static DMRouterProperties dmRouter=new DMRouterProperties();;
	private final static HashMap<String, Object> DMRouterMap = new HashMap<String, Object>(25);
	private final static String DMJNDIPropertyName = "dmrouter";
	
	private ResourceBundle dmRouterBundle = null;
	
	private DMRouterProperties()
	{
		
	}
	
	public final static DMRouterProperties getInstance()
	{
		return dmRouter;
	}
	
	public void init()
	{
		loadRouterProperties();
		parseRouterConfiguration();
	}
	
	private void loadRouterProperties()
	{
		dmRouterBundle = ResourceBundle.getBundle(DMJNDIPropertyName);
		Spool.debug("Loaded DMJNDI Properties");
	}
	
	private void setDMRouterMap(String keyName,Object value)
	{
		DMRouterMap.put(keyName, value);
	}
	
	public DataSource getDMDataSource(String dbName)
	{
		DataSource ds=null;
		ds=(DataSource)DMRouterMap.get(dbName+"_JNDI");
		if(ds==null)
		{
			Spool.warn("Unable get DataSource for["+dbName+"] ");
		}	
		return ds;
	}
	
	private void parseRouterConfiguration()
	{
		Enumeration<String> routerKeyEnum =null;
		String keyName=null;
		String value=null;
		routerKeyEnum=dmRouterBundle.getKeys();
		while(routerKeyEnum.hasMoreElements())
		{
			keyName = routerKeyEnum.nextElement();
			value = dmRouterBundle.getString(keyName);
			if(keyName.contains("_JNDI"))
			{
				loadDataSource(value,keyName);
			}else if(keyName.contains("_DMCFG"))
			{
				loadProperties(value,keyName);
			}else if(keyName.contains("_REQDTD")||keyName.contains("_RESDTD"))
			{
				setDMRouterMap(keyName,value);
			}else
			{
				Spool.error("Invalid Key Name& Value["+keyName+"]["+value+"]");
			}
		}
	}
	
	private void loadProperties(String proName,String proValue)
	{
		Properties property=null;
		property = new Properties();
		try 
		{
			property.load(new FileInputStream(proName));
			setDMRouterMap(proValue,property);
		} catch (FileNotFoundException e) 
		{
			Spool.error("Unable Load Property["+proName+"]["+proValue+"]|"+e);
		} catch (IOException e) 
		{
			Spool.error("Unable Load Property["+proName+"]["+proValue+"]|"+e);
		}
		
	}
	
	private final static String[] getTxnEntries(String code,String key)
	{
				
		String[] routerEntries=null;
		Properties properties=null;
		properties=(Properties)DMRouterMap.get(key+"_DMCFG");
		routerEntries = properties.getProperty(code).split("\\|");
		return routerEntries;
	}
	public final String getReqDTDPath(String txnKey)
	{
		return (String)DMRouterMap.get(txnKey+"_REQDTD");
	}
	
	public final String getResDTDPath(String txnKey)
	{
		return (String)DMRouterMap.get(txnKey+"_RESDTD");
	}
	
	public final static DMTxnParams getDMTxnParams(String txnCode,String routerKey)
	{
		DMTxnParams dmRouterEntry = new DMTxnParams();
		String[] routerEntries = getTxnEntries(txnCode,routerKey);
		dmRouterEntry.txnName = routerEntries[1];
		dmRouterEntry.txnType = Integer.parseInt(routerEntries[2]);
		dmRouterEntry.txnDtdFilename = "NULL".equals(routerEntries[3]) ? null: routerEntries[3];
		dmRouterEntry.txnOutputRes = "NULL".equals(routerEntries[4]) ? null: routerEntries[4];
		dmRouterEntry.txnCode = "NULL".equals(routerEntries[5]) ? null : routerEntries[5];
		dmRouterEntry.txnOnetimeRes = "NULL".equals(routerEntries[6]) ? null: routerEntries[6];
		dmRouterEntry.MutiInpDelim = "NULL".equals(routerEntries[7]) ? null: routerEntries[7];

		return dmRouterEntry;
	}
	
	private void loadDataSource(String dbjndi,String dbname)
	{
		InitialContext context=null;
		DataSource dataSource=null;

		try {
			context=new InitialContext();
			dataSource=(DataSource)context.lookup(dbjndi);
			if(dataSource==null)
				Spool.warn("Datasource 0 is Null");
			setDMRouterMap(dbname,dataSource);
			Spool.trace("DataSource for["+dbjndi+"]created and loaded with["+dbname+"]as key");

		} catch (NamingException namingException) 
		{
			Spool.error("Exception while loading Datasource|"+namingException);
		}
	}
	
	public void closeDBElements(ResultSet rs,Statement stmt,Connection con)
	{
		Spool.trace("Clossing the ResultSet, Statement and Connection");
    	try{ 
    		if(rs!=null){
    			rs.close();
    			rs=null;
    			Spool.trace("Clossed the ResultSet");
    		}
    		if(stmt!=null){
    			stmt.close();
    			stmt=null;
    			Spool.trace("Clossed the Statement");
    		}
    		if(con!=null){
    			con.close();
    			con=null;
    			Spool.trace("Clossed the Connection");
    		}
    	}catch(SQLException sqe){
    		Spool.error("SQL EXception while closing the DB Objects|"+sqe);
    	}    	
	}
}
