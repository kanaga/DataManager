package com.ycs.dm.helpers;

import java.util.ResourceBundle;

import org.apache.log4j.Logger;

public final class DMTxnLogProperties 
{
	private static final Logger logger = Logger.getLogger(DMTxnLogProperties.class);
	private final static ResourceBundle dmAppEntries = ResourceBundle.getBundle("dmtxnlog");
	
	public static String TXNID_FMT;
	public static String TXNCODE_FMT;
	public static String NETID_FMT;
	public static String TXNTYPE_FMT;
	public static String TXNERRCODE_FMT;
	public static String TXNSTATUS_FMT;
    public static String MSGTYPE_FMT;
    
	public static String TXNID_XPR;
	public static String TXNCODE_XPR;
	public static String NETID_XPR;
	public static String TXNTYPE_XPR;
	public static String TXNERRCODE_XPR;
	public static String TXNSTATUS_XPR;

	private DMTxnLogProperties()
	{

	}
	public final static void init()
	{
		logger.debug("Start Initializing TXN Log Properties");
		TXNID_FMT= dmAppEntries.getString("TXNID_FMT");
		MSGTYPE_FMT=dmAppEntries.getString("MSGTYPE_FMT");
		TXNCODE_FMT= dmAppEntries.getString("TXNCODE_FMT");
		NETID_FMT= dmAppEntries.getString("NETID_FMT");
		TXNTYPE_FMT= dmAppEntries.getString("TXNTYPE_FMT");
		TXNERRCODE_FMT= dmAppEntries.getString("TXNERRCODE_FMT");
		TXNSTATUS_FMT= dmAppEntries.getString("TXNSTATUS_FMT");

		TXNID_XPR= dmAppEntries.getString("TXNID_XPR");
		TXNCODE_XPR= dmAppEntries.getString("TXNCODE_XPR");
		NETID_XPR= dmAppEntries.getString("NETID_XPR");
		TXNTYPE_XPR= dmAppEntries.getString("TXNTYPE_XPR");
		TXNERRCODE_XPR= dmAppEntries.getString("TXNERRCODE_XPR");
		TXNSTATUS_XPR= dmAppEntries.getString("TXNSTATUS_XPR");
		logger.debug("Initializing TXN Log Properties Done");
	}






}
