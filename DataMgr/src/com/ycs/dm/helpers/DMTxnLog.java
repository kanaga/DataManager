package com.ycs.dm.helpers;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public final class DMTxnLog
{
	private static final Logger TXNLOG = Logger.getLogger(DMTxnLog.class);
	private static final DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
	
	private static XPathFactory xpathfactory = XPathFactory.newInstance();
	private static final InputSource xmlSource = new InputSource();
	
	private static final StringBuilder fmtOutput = new StringBuilder(30);
	
	private static DocumentBuilder docBuilder;
	private static XPath xpath;
	
	private static XPathExpression txnidxpr;
	private static XPathExpression txncodexpr;
	private static XPathExpression netidxpr;
	private static XPathExpression txntypexpr;
	private static XPathExpression txnerrcodeexpr;
	private static XPathExpression txnstatusexpr;
	
	
	private DMTxnLog()
	{}
	
	static
	{
		try 
		{
			docBuilder = docBuilderFactory.newDocumentBuilder();
			xpath = xpathfactory.newXPath();
			
			txnidxpr = xpath.compile(DMTxnLogProperties.TXNID_XPR);
			txncodexpr = xpath.compile(DMTxnLogProperties.TXNCODE_XPR);
			netidxpr = xpath.compile(DMTxnLogProperties.NETID_XPR);
			txntypexpr = xpath.compile(DMTxnLogProperties.TXNTYPE_XPR);
			txnerrcodeexpr = xpath.compile(DMTxnLogProperties.TXNERRCODE_XPR);
			txnstatusexpr = xpath.compile(DMTxnLogProperties.TXNSTATUS_XPR);
			
		} catch (XPathExpressionException e) 
		{
			TXNLOG.error("Problem in Loading XPath Expression:-"+e);
		} catch (ParserConfigurationException e) {
			TXNLOG.error("Problem in Creating new DocumentBuilder:-"+e);
		}
	}
		
	public static void print(String reqXML,String dbName,long elapsedTime) 
	{
		Document xmlDoc = null;
		
		String txnid = null;
		String txncode = null;
		String netid = null;
		String txntype = null;
		String txnerrcode = null;
		String txnstatus = null;
		
		try
		{
			xmlSource.setCharacterStream(new StringReader(reqXML));
			xmlDoc = docBuilder.parse(xmlSource);
		
      	    txnid = (String)txnidxpr.evaluate(xmlDoc, XPathConstants.STRING);
      	    txncode = (String)txncodexpr.evaluate(xmlDoc, XPathConstants.STRING);
      	    netid = (String)netidxpr.evaluate(xmlDoc, XPathConstants.STRING);
      	    txntype = (String)txntypexpr.evaluate(xmlDoc, XPathConstants.STRING);
      	    txnerrcode = (String)txnerrcodeexpr.evaluate(xmlDoc, XPathConstants.STRING);
      	    txnstatus = (String)txnstatusexpr.evaluate(xmlDoc, XPathConstants.STRING);
	  String xmlType=String.format(DMTxnLogProperties.TXNTYPE_FMT, txntype);
	  String mesage="";
	  if(xmlType.equalsIgnoreCase("01|"))
	  {
		  mesage="REQ"; 
	  }else if(xmlType.equalsIgnoreCase("02|"))
	  {
		  mesage="RES"; 
	  }
			fmtOutput.append(String.format(DMTxnLogProperties.TXNTYPE_FMT, txntype))
			.append(String.format(DMTxnLogProperties.MSGTYPE_FMT,mesage))
			.append(String.format(DMTxnLogProperties.TXNCODE_FMT, txncode))
			.append(String.format(DMTxnLogProperties.TXNID_FMT, txnid))
   			.append(String.format(DMTxnLogProperties.NETID_FMT, netid))
   			.append(dbName +" | ")
   			.append(String.format(DMTxnLogProperties.TXNERRCODE_FMT, txnerrcode))
   			.append(String.format(DMTxnLogProperties.TXNSTATUS_FMT, txnstatus));
	   		
			if(elapsedTime!=0){
				fmtOutput.append(elapsedTime+"ms");
			}
	   		
			TXNLOG.info(fmtOutput.toString().trim()); 
			fmtOutput.delete(0, fmtOutput.length());
			
		}catch(SAXException saxException)
		{
			TXNLOG.error("Problem in Loading XML Message|"+saxException);
		} catch (IOException ioException) 
		{
			TXNLOG.error("Problem in Loading XML Message|"+ioException);
		} catch (XPathExpressionException xPathExpressionException) 
		{
			TXNLOG.error("Problem in Loading XPath Expression|"+xPathExpressionException);
		}
		finally
		{
			xmlDoc = null;
			txnid = null;
			txncode = null;
			netid = null;
			txntype = null;
			txnerrcode = null;
			txnstatus = null;
		}
	}
	public static String getXMLParsedata(String reqXML) 
	{
		Document xmlDoc = null;
		String txnid = null;
		String txncode = null;
		String netid = null;
		String txntype = null;
		String txnerrcode = null;
		String txnstatus = null;
		String outputData = null;
		try
		{
			xmlSource.setCharacterStream(new StringReader(reqXML));
			xmlDoc = docBuilder.parse(xmlSource);
		
      	    txnid = (String)txnidxpr.evaluate(xmlDoc, XPathConstants.STRING);
      	    txncode = (String)txncodexpr.evaluate(xmlDoc, XPathConstants.STRING);
      	    netid = (String)netidxpr.evaluate(xmlDoc, XPathConstants.STRING);
      	    txntype = (String)txntypexpr.evaluate(xmlDoc, XPathConstants.STRING);
      	   
			fmtOutput.append(String.format(DMTxnLogProperties.TXNCODE_FMT, txncode))
			.append(String.format(DMTxnLogProperties.TXNID_FMT, txnid))
   			.append(String.format(DMTxnLogProperties.NETID_FMT, netid));
	   		
			outputData=fmtOutput.toString();
			//TXNLOG.info(fmtOutput.toString()); 
			fmtOutput.delete(0, fmtOutput.length());
			
		}catch(SAXException saxException)
		{
			TXNLOG.error("Problem in Loading XML Message|"+saxException);
		} catch (IOException ioException) 
		{
			TXNLOG.error("Problem in Loading XML Message|"+ioException);
		} catch (XPathExpressionException xPathExpressionException) 
		{
			TXNLOG.error("Problem in Loading XPath Expression|"+xPathExpressionException);
		}
		finally
		{
			xmlDoc = null;
			txnid = null;
			txncode = null;
			netid = null;
			txntype = null;
			txnerrcode = null;
			txnstatus = null;
		}
		return outputData;
	}
}

