package com.ycs.dm.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.apache.log4j.Logger;

import com.ycs.dm.DataManager;
import com.ycs.dm.helpers.DMTxnLogProperties;
import com.ycs.dm.helpers.DMRouterProperties;
import com.ycs.dm.helpers.DMTxnLog;

/**
 * Servlet implementation class DataManagerServlet
 */
public class DMServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
	private static final Logger Spool = Logger.getLogger(DMServlet.class);
	private String requestTime;
	private String responseTime;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DMServlet() 
    {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException 
	{
		DMTxnLogProperties.init();
		DMRouterProperties.getInstance().init();
		Spool.debug("Initialized Application Configurations");
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy()
	{
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		//Spool.warn("Invalid Method Call for the Request");
		
		doPost(request, response);
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		
		String requestXML=null;
		String requestDBName=null;
		String responseXML=null;
		Spool.trace("Request Map"+request.getParameterMap());
		requestXML=request.getParameter("REQUEST");
		requestDBName=request.getParameter("REQUESTDB");		
		Spool.trace("Request Received for DM Congig BaseKey["+requestDBName+"]");
		Spool.debug("FE Request XML["+requestXML+"]");
		
		 requestTime = new SimpleDateFormat("HH:mm:ss:SSS").format(Calendar.getInstance().getTime());
		//System.out.println(requestTime);
		DMTxnLog.print(requestXML,requestDBName,0);
		
		responseXML=getPLSQLResponse(requestXML,requestDBName);
		Spool.trace("Sending Response for DBNAME["+requestDBName+"]");
		
		responseTime= new SimpleDateFormat("HH:mm:ss:SSS").format(Calendar.getInstance().getTime());
		Spool.trace("FE Response XML["+responseXML+"]");
		DMTxnLog.print(responseXML,requestDBName,getDifference(requestTime,responseTime));
		
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/xml");
		PrintWriter responseWriter = response.getWriter();
		responseWriter.println(responseXML);
		responseWriter.close();
		

	}
	
	public static long getDifference(String time1, String time2){
		SimpleDateFormat formater = new SimpleDateFormat("HH:mm:ss:SSS");
		Date fromDate = null;
		Date toDate = null;
		try {
			fromDate = formater.parse(time1);
			toDate = formater.parse(time2);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		long difference = toDate.getTime() - fromDate.getTime(); 
		return difference;
	}
		
	private String getPLSQLResponse(String inputxml,String requestDBName) 
	{
		String htmlString=null;
		DataManager dm = null;
		try 
		{
			dm = new DataManager(requestDBName);
			htmlString = dm.createStrfrmXml(inputxml);
			dm=null;
		
		} catch (TransformerFactoryConfigurationError e)
		{
			Spool.error("Exception 0:-" + e);
		}
		if(htmlString==null)
		{
			htmlString="ERROR";
		}
		return htmlString;
	}

}
